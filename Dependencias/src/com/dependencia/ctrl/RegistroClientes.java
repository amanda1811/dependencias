/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dependencia.ctrl;

import com.dependencia.impl.Clienteimpl;
import com.dependencia.model.Cliente;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequestScoped
@ManagedBean
public class RegistroClientes implements Serializable {

    @Autowired
    private Clienteimpl clienteDao;
    private Cliente cliente;
    private List<Cliente> listaCliente;
    String mensaje = "";

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<Cliente> getListaCliente() {
        this.listaCliente = clienteDao.findAll();
        return listaCliente;
    }

    public void setListaCliente(List<Cliente> listaCliente) {
        this.listaCliente = listaCliente;
    }

    @PostConstruct
    public void init() {
        cliente = new Cliente();
    }

    public void crearCliente() {
        try {
            clienteDao.create(cliente);
            mensaje = "Cliente Creado";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }

        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void editarCliente() {
        try {
            clienteDao.edit(cliente);
            mensaje = "Cliente Editado";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }

        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void eliminarCliente(Cliente c) {
        try {
            clienteDao.remove(c);
            mensaje = "Cliente Eliminado";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }

        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void cargarCliente(Cliente c) {
        cliente = c;
    }
}
