/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dependencia.ctrl;

import com.dependencia.conexion.Conexion;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import org.springframework.stereotype.Component;

@Component
@RequestScoped
@ManagedBean
public class DetalleProductos implements Serializable {

    public void mostrarDetalles() throws JRException, IOException {

        File jasper = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("exports/DetalleProducto.jasper"));
        Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("param", "jas");
        JasperPrint jasperprint = JasperFillManager.fillReport(jasper.getPath(), parametros, Conexion.conectar());

        //Visualizar en el navegador
        byte[] bytes = JasperRunManager.runReportToPdf(jasper.getPath(), parametros, Conexion.conectar());
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        response.setContentType("application/pdf");
        response.setContentLength(bytes.length);
        ServletOutputStream os = response.getOutputStream();
        os.write(bytes, 0, bytes.length);
        os.flush();
        os.close();
        FacesContext.getCurrentInstance().responseComplete();
    }
    
//    Descargar PDF con JSF
//    HttpServletResponse response= (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
//    response.addHeader("Content-disposition", "attachment; filename=reporte.pdf");
//    ServletOutputStream printer = response.getOutputStream();
//    JasperExportManager.exportReportToPdfStream(jasperprint, printer);
//    FacesContext.getCurrentInstance().responseComplete();

}
