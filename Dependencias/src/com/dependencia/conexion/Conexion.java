/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dependencia.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Conexion {
    
    public static Connection c;
    
    public static Connection conectar(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            c= DriverManager.getConnection("jdbc:mysql://usam-sql.sv.cds:3306/facturacion?useSSL=false", "kz", "kzroot");
            PreparedStatement p= c.prepareStatement("select * from detalle order by num_detalle;");
            ResultSet r= p.executeQuery();
        } catch (SQLException e) {
            c= null;
        }catch(ClassNotFoundException x){
            c= null;
        
        }
        return c;
        
    }
    
    public static void desconectar(){
        try {
            if(c != null){
            if(!c.isClosed()){
                c.close();
            
            }
            }
        } catch (SQLException e) {
            
        }
    }
    
}
