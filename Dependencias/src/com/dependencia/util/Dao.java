/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dependencia.util;

import java.util.List;

/**
 *
 * @author amanda.ayalausam
 */
public interface Dao<T> {
    
    public void create(T e);
    
    public void edit(T e);
    
    public void remove(T e);
    
    public List<T> findAll();
    
    public T find(Object e);
    
}
