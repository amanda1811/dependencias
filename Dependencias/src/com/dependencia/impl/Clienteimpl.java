/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dependencia.impl;

import com.dependencia.model.Cliente;
import com.dependencia.util.AbstractFacade;
import com.dependencia.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class Clienteimpl extends AbstractFacade<Cliente> implements Dao<Cliente> {

    @Autowired
    private SessionFactory SF;

    public Clienteimpl() {
        super(Cliente.class);
    }

    public Clienteimpl(SessionFactory sf, Class<Cliente> entityClass) {
        super(entityClass);
    }

    @Override
    protected SessionFactory sessionFactory() {
        return SF;
    }
}
